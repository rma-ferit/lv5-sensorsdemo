package hr.ferit.brunozoric.sensorsdemo

import android.content.Context
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {

    private val TAG = "Sensors"

    private val decimalFormat = DecimalFormat("0.00")
    private lateinit var sensorManager: SensorManager;
    private lateinit var accelerationSensor: Sensor

    private val sensorListener = object: SensorEventListener{
        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        }
        override fun onSensorChanged(event: SensorEvent?) {
            val values = event?.values ?: floatArrayOf(0.0f, 0.0f, 0.0f)
            updateUi(values)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
    }

    override fun onResume() {
        super.onResume()
        sensorManager.registerListener(sensorListener, accelerationSensor, SensorManager.SENSOR_DELAY_UI)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(sensorListener)
    }

    private fun updateUi(values: FloatArray) {

        val totalAcceleration = Math.sqrt(values.map { x -> x*x }.sum().toDouble())
        accelerationDisplay.text = decimalFormat.format(totalAcceleration)
        if(totalAcceleration > 12)
            rootView.setBackgroundResource(R.color.colorHigh)
        else
            rootView.setBackgroundResource(R.color.colorLow)

        accelerationComponents.text = ""
        values.forEach { accelerationComponents.append("${decimalFormat.format(it)}\n") }
    }
}
